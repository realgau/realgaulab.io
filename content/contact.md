---
title: Contact
featured_image: ""
omit_header_text: true
description: Contactez-nous.
type: page
menu: main

---


<form
  action="https://formspree.io/mpzrzvej"
  method="POST"
>
  <label>
    Votre adresse :
    <input type="text" name="_replyto">
  </label>
  <label>
   Votre message :
    <textarea name="message"></textarea>
  </label>

  <!-- your other form fields go here -->

  <button type="submit">Envoyer</button>
</form>