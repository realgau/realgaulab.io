---
title: "Doc.In."
featured_image: ''
description: "Flux de production éditoriale, accessibilité et pratiques documentaires."
---
Je suis Gautier, concepteur de solutions et de productions éditoriales inclusives et adaptées. 

**Doc.In** est un espace de réflexion sur les pratiques documentaires, l'histoire des métiers du livre et l'accessibilité de la lecture.

