---
title: "À propos"
description: "Chief documentation officer"
featured_image: ''
---
{{< figure src="/images/ea_separator_2.png" title="" >}}
## Chief documentation officer
Passionné par les univers du livre et de la lecture, j’ai traversé vingt années d’évolutions techniques dans des positions variées, du micro projet sans but lucratif à l’activité d’institutions internationales en passant par les groupes d’édition commerciale et l’édition associative. Ces dernières années, j’ai eu l’occasion de transmettre mon savoir à travers une activité de formateur qui m’a amené à porter un regard critique sur mes pratiques de production et m’a appris à les détailler puis souvent à les réajuster et à confronter aspects théoriques et mises en pratique.

Ce sont ces aspects de réflexion, d’expérimentation et de mise en pratique argumentée que je souhaite ici approfondir.

{{< figure src="/images/ea_separator_2.png" title="" >}}

## Sous le capot
Ce site est construit dynamiquement par [Hugo](https://gohugo.io/). Il est hébergé par [Gitlab](https://gitlab.com/).




